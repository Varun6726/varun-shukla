#include<stdio.h>
void input(int *,int *,int *);
void cmp(int *,int *,int *,int *,float *,int *,int *);
void output(int *,float *,int *,int *);
int main()
{
    int a,b,c,t,B,S;
    float A;
    input(&a,&b,&c);
    cmp(&a,&b,&c,&t,&A,&B,&S);
    output(&t,&A,&B,&S);
    return 0;
}
void input(int *a,int *b,int *c)
{
    int x,y,z;
    printf("enter three no.s\n");
    scanf("%d%d%d",&x,&y,&z);
    *a=x;
    *b=y;
    *c=z;
}
void cmp(int *a,int *b,int *c,int *t,float *A,int *B,int *S)
{
    *t=*a+*b+*c;
    *A=(float)*t/3;
    *B=(*a>*b)?((*a>*c)?*a:*c):((*b>*c)?*b:*c);
    *S=(*a<*b)?((*a<*c)?*a:*c):((*b<*c)?*b:*c);    
}    
void output(int *t,float *A,int *B,int *S)
{
    printf("total is %d\n",*t);
    printf("avg is %f\n",*A);
    printf("greatest is %d\n",*B);
    printf("smallest is %d\n",*S);
}